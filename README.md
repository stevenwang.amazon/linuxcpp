![linuxcpp logo](logo.png "Linux Cpp Logo")

[![Codacy](https://img.shields.io/codacy/e27821fb6289410b8f58338c7e0bc686.svg)](https://gitlab.com/stevenwang.amazon)
[![Gemnasium](https://img.shields.io/gemnasium/mathiasbynens/he.svg)](https://gitlab.com/stevenwang.amazon)


This is a test for readme.md on GitLab. Good example: [grape on GitHub](https://github.com/intridea/grape)

Project files are automtically pushed by a bash file to GitLab: 3 args

## Format Readme File
------------------------

Format readme file to introduce the project.


### GitLab Flavored Markdown
------------------------

Descriptions, issues, comments and pull requests are written by using GitLab Flavored Markdown. For more information on this, refer to the [GitLab Flavored Markdown](https://gitlab.com/gitlab-org/gitlab-ce/blob/6-4-stable/doc/markdown/markdown.md)


### Text Style 
------------------------

Bold: **Google**


### Blockquotes
-----------------------------

> Blockquotes are very handy in email to emulate reply text.


### Code and Syntax Highlighting
------------------------

Two kinds of code and syntax highlighting:
1. Inline `code` has `back-ticks around` it: `#include <iostream>`
2. Blocks of code are either fenced by lines with three back-ticks, or are indented with four spaces. 

C++ code syntax highlight:

```c++
#include <iostream>

int main() {
	printf( "Hello, GitLab!" );
}
```

shell highlight:

```bash
$ ls -l
$ cd myfolder 
```


### Lists
-----------------------------

Ordered List
1. Number 1
	1. ordered sub-list
2. Number 2
	* unordered sub-list

Unordered List
- GitLab
	+ Free
	+ Private
- GitHub


### Images
-----------------------------

Insert images:
![alt text](testimage.png "description on the image")

Shields images: [Shields.io](http://shields.io/) 
- Example:
	+ [![Codacy](https://img.shields.io/codacy/e27821fb6289410b8f58338c7e0bc686.svg)](https://gitlab.com/stevenwang.amazon) 
	+ [![Gemnasium](https://img.shields.io/gemnasium/mathiasbynens/he.svg)](https://gitlab.com/stevenwang.amazon)


### Links
-----------------------------

Create links:
[GitLab Flavored Markdown](https://gitlab.com/gitlab-    org/gitlab-ce/blob/6-4-stable/doc/markdown/markdown.md)

